package com.sda.service;

import com.sda.data.Data;
import com.sda.model.Car;
import com.sda.model.Person;

public class PrintingService {

    public void printAllPeople() {
        for (Person person : Data.getPeople()) {
            System.out.println(person);
        }

    }

    public void printAllCars() {
        for (Car car : Data.getCars()) {
            System.out.println(car);
        }
    }
}
