package com.sda;

import com.sda.service.PrintingService;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world !");

        PrintingService screenPrinter = new PrintingService();
        screenPrinter.printAllPeople();
        screenPrinter.printAllCars();
    }
}
